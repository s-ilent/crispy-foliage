# Crispy Foliage

A shader for Unity/VRchat that's designed for rendering semitransparent plants and plant-like objects with thin details that blow in the wind.

![Example image](https://gitlab.com/s-ilent/crispy-foliage/-/wikis/uploads/f0a56b82b1d61f2534c79684262fbe1e/Unity_2021-02-04_14-19-44c.png.jpg)

## Installation

Download the repository. Then place the Shader/ folder with the shader into your Assets/ directory.

## Usage

This shader supports many of the same features as Standard, but there are some differences.

Options like Normal Map, Specular Map, and Transmission Map will only be used if enabled. 

Cutout will set the point at which transparency is treated as fully transparent, while Transparency Threshold will set the point at which the alpha is treated as fully opaque.

When the Specular Map is disabled, the Metallic Scale controls the metalness of the material and metalness shading is used. When the Specular Map is enabled, specular shading is used. 

The Transmission Map and settings control the strength and power of back lighting. 

Wind Pinning specifies what parts of the mesh don't move. If Texture isn't selected, the texture isn't used.

Wave and Distance Properties correspond to the following
* Wind Speed
* Wave Size
* Wind Amount
* Max Distance (interval of movement)

## License?

This work is licensed under the MIT license.